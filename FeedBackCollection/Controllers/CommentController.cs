﻿
using FeedBack.Entity;
using FeedBack.Manager.FeedBackCollection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FeedBackCollection.Controllers
{
    public class CommentController : Controller
    {
        FeedBackCollectionManager feedBackCollectionManager = new FeedBackCollectionManager();

        #region Posts
        public ActionResult GetAllPosts()
        {
            List<Post> posts = feedBackCollectionManager.GetPosts();
            ViewBag.Posts = posts;
            return View();
        }
        #endregion


        #region Comments
        public ActionResult AddComment(Comments comments)
        {
            int result = feedBackCollectionManager.AddComment(comments);
            if( result > 0)
            {
                TempData["msg"] = "<script>alert('Successfull');</script>";
            }
            else
            {
                TempData["msg"] = "<script>alert('Failed');</script>";
            }
            List<Post> posts = feedBackCollectionManager.GetPosts();
            ViewBag.Posts = posts;
            return View("GetAllPosts");
        }
        public ActionResult GetCommentsByPosts(int PostId)
        {
            List<Comments> comments = feedBackCollectionManager.GetCommentsByPosts(PostId);
            ViewBag.Comments = comments;
            return View();
        }
        #endregion


        #region FeedBack
        public ActionResult AddFeedBack(FeedBackCount feedBack)
        {
            int result = feedBackCollectionManager.AddFeedBack(feedBack);
            if (result > 0)
            {
                TempData["msg"] = "<script>alert('Successfull');</script>";
            }
            else
            {
                TempData["msg"] = "<script>alert('Failed');</script>";
            }
            List<Comments> comments = feedBackCollectionManager.GetCommentsByPosts(feedBack.PostId);
            ViewBag.Comments = comments;
            return View("GetCommentsByPosts");
        }

        #endregion

    }
}