﻿using FeedBack.Entity;
using FeedBack.Manager.FeedBackCollection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FeedBackCollection.Controllers.Api_Controller
{
    
    public class FeedbackApiController : ApiController
    {

        FeedBackCollectionManager feedBackCollectionManager = new FeedBackCollectionManager();
        // GET: api/FeedbackApi
        public IEnumerable<Post> Get()
        {
            return feedBackCollectionManager.GetPostsWithDetails();
        }

        // GET: api/FeedbackApi/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/FeedbackApi
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/FeedbackApi/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/FeedbackApi/5
        public void Delete(int id)
        {
        }
    }
}
