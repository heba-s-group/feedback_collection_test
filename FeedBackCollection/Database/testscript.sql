USE [DbTestComments]
GO
/****** Object:  Table [dbo].[tbl_Comment]    Script Date: 10/22/2020 1:12:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Comment](
	[CommentId] [int] IDENTITY(1,1) NOT NULL,
	[CommentMsg] [nvarchar](max) NULL,
	[PostId] [int] NULL,
	[UserId] [int] NULL,
	[CreateDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Comment] PRIMARY KEY CLUSTERED 
(
	[CommentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_FeedBack]    Script Date: 10/22/2020 1:12:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_FeedBack](
	[FeedBackId] [int] IDENTITY(1,1) NOT NULL,
	[CommentId] [int] NULL,
	[Interest] [int] NULL,
	[UserId] [int] NULL,
	[PostId] [int] NULL,
	[CreateDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_CommentVote] PRIMARY KEY CLUSTERED 
(
	[FeedBackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Post]    Script Date: 10/22/2020 1:12:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Post](
	[PostId] [int] IDENTITY(1,1) NOT NULL,
	[PostMessage] [nvarchar](max) NULL,
	[CreateDate] [datetime] NULL,
	[UserId] [int] NULL,
 CONSTRAINT [PK_tbl_Post] PRIMARY KEY CLUSTERED 
(
	[PostId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_User]    Script Date: 10/22/2020 1:12:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](max) NULL,
 CONSTRAINT [PK_tbl_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[tbl_Comment] ON 

INSERT [dbo].[tbl_Comment] ([CommentId], [CommentMsg], [PostId], [UserId], [CreateDate]) VALUES (6, N'nice', 1, 1, CAST(N'2020-10-21 13:55:50.000' AS DateTime))
INSERT [dbo].[tbl_Comment] ([CommentId], [CommentMsg], [PostId], [UserId], [CreateDate]) VALUES (7, N'good', 1, 2, CAST(N'2020-10-21 13:57:01.000' AS DateTime))
INSERT [dbo].[tbl_Comment] ([CommentId], [CommentMsg], [PostId], [UserId], [CreateDate]) VALUES (8, N'bad', 1, 3, CAST(N'2020-10-21 13:58:00.000' AS DateTime))
INSERT [dbo].[tbl_Comment] ([CommentId], [CommentMsg], [PostId], [UserId], [CreateDate]) VALUES (9, N'bad', 2, 4, CAST(N'2020-10-21 13:58:42.000' AS DateTime))
INSERT [dbo].[tbl_Comment] ([CommentId], [CommentMsg], [PostId], [UserId], [CreateDate]) VALUES (10, N'bad', 3, 5, CAST(N'2020-10-21 13:59:09.000' AS DateTime))
INSERT [dbo].[tbl_Comment] ([CommentId], [CommentMsg], [PostId], [UserId], [CreateDate]) VALUES (11, N'ok', 2, 5, CAST(N'2020-10-21 13:59:45.000' AS DateTime))
INSERT [dbo].[tbl_Comment] ([CommentId], [CommentMsg], [PostId], [UserId], [CreateDate]) VALUES (12, N'yes', 3, 1, CAST(N'2020-10-21 14:00:33.000' AS DateTime))
INSERT [dbo].[tbl_Comment] ([CommentId], [CommentMsg], [PostId], [UserId], [CreateDate]) VALUES (13, N'true', 3, 3, CAST(N'2020-10-21 14:00:51.000' AS DateTime))
INSERT [dbo].[tbl_Comment] ([CommentId], [CommentMsg], [PostId], [UserId], [CreateDate]) VALUES (14, N'nice', 2, 1, CAST(N'2020-10-21 14:02:13.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_Comment] OFF
SET IDENTITY_INSERT [dbo].[tbl_FeedBack] ON 

INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (1, 6, 0, 2, 1, CAST(N'2020-10-21 14:17:30.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (2, 6, 1, 1, 1, CAST(N'2020-10-21 14:18:33.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (3, 6, 1, 3, 1, CAST(N'2020-10-21 14:19:40.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (4, 6, 0, 4, 1, CAST(N'2020-10-21 14:20:31.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (5, 6, 1, 5, 1, CAST(N'2020-10-21 14:20:31.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (6, 7, 0, 4, 1, CAST(N'2020-10-21 14:21:22.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (7, 7, 1, 1, 1, CAST(N'2020-10-21 14:25:42.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (8, 7, 1, 2, 1, CAST(N'2020-10-21 14:26:28.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (9, 7, 1, 3, 1, CAST(N'2020-10-21 14:26:49.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (10, 8, 1, 3, 1, CAST(N'2020-10-21 14:27:09.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (11, 8, 0, 2, 1, CAST(N'2020-10-21 14:27:09.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (12, 8, 1, 5, 1, CAST(N'2020-10-21 14:27:09.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (13, 8, 1, 4, 1, CAST(N'2020-10-21 14:27:09.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (14, 9, 1, 3, 2, CAST(N'2020-10-21 14:28:24.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (15, 11, 0, 5, 2, CAST(N'2020-10-21 14:28:47.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (16, 9, 1, 5, 2, CAST(N'2020-10-21 14:28:53.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (17, 14, 1, 4, 2, CAST(N'2020-10-21 14:28:56.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (18, 9, 1, 1, 2, CAST(N'2020-10-21 14:28:53.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (19, 9, 1, 2, 2, CAST(N'2020-10-21 14:28:53.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (20, 11, 0, 3, 2, CAST(N'2020-10-21 14:28:47.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (21, 11, 1, 4, 2, CAST(N'2020-10-21 14:28:47.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (22, 14, 1, 1, 2, CAST(N'2020-10-21 14:28:56.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (23, 14, 1, 2, 2, CAST(N'2020-10-21 14:28:56.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (24, 14, 1, 3, 2, CAST(N'2020-10-21 14:28:56.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (25, 10, 0, 5, 3, CAST(N'2020-10-21 14:32:23.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (26, 12, 0, 5, 3, CAST(N'2020-10-21 14:32:25.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (27, 13, 0, 5, 3, CAST(N'2020-10-21 14:32:27.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (28, 10, 0, 4, 3, CAST(N'2020-10-21 14:32:23.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (29, 10, 0, 3, 3, CAST(N'2020-10-21 14:32:23.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (30, 10, 0, 2, 3, CAST(N'2020-10-21 14:32:23.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (31, 12, 1, 3, 3, CAST(N'2020-10-21 14:32:25.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (32, 12, 1, 2, 3, CAST(N'2020-10-21 14:32:25.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (33, 12, 1, 1, 3, CAST(N'2020-10-21 14:32:25.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (34, 13, 1, 1, 3, CAST(N'2020-10-21 14:32:27.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (35, 13, 1, 2, 3, CAST(N'2020-10-21 14:32:27.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (36, 13, 1, 3, 3, CAST(N'2020-10-21 14:32:27.000' AS DateTime))
INSERT [dbo].[tbl_FeedBack] ([FeedBackId], [CommentId], [Interest], [UserId], [PostId], [CreateDate]) VALUES (37, 13, 1, 4, 3, CAST(N'2020-10-21 14:32:27.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_FeedBack] OFF
SET IDENTITY_INSERT [dbo].[tbl_Post] ON 

INSERT [dbo].[tbl_Post] ([PostId], [PostMessage], [CreateDate], [UserId]) VALUES (1, N'Hello! This is a test message.', CAST(N'2020-10-21 12:55:50.000' AS DateTime), 1)
INSERT [dbo].[tbl_Post] ([PostId], [PostMessage], [CreateDate], [UserId]) VALUES (2, N'This is a sample post', CAST(N'2020-10-21 13:50:50.000' AS DateTime), 2)
INSERT [dbo].[tbl_Post] ([PostId], [PostMessage], [CreateDate], [UserId]) VALUES (3, N'Todays weather is good', CAST(N'2020-10-21 12:25:50.000' AS DateTime), 3)
SET IDENTITY_INSERT [dbo].[tbl_Post] OFF
SET IDENTITY_INSERT [dbo].[tbl_User] ON 

INSERT [dbo].[tbl_User] ([UserId], [UserName]) VALUES (1, N'Limon')
INSERT [dbo].[tbl_User] ([UserId], [UserName]) VALUES (2, N'Tania')
INSERT [dbo].[tbl_User] ([UserId], [UserName]) VALUES (3, N'Maruf')
INSERT [dbo].[tbl_User] ([UserId], [UserName]) VALUES (4, N'Hossain')
INSERT [dbo].[tbl_User] ([UserId], [UserName]) VALUES (5, N'Labib')
SET IDENTITY_INSERT [dbo].[tbl_User] OFF
