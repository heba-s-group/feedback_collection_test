﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FeedBack.Entity;

namespace FeedBack.Entity
{
    public class Post
    {
        public int PostId { get; set; }
        public string PostMessage { get; set; }
        public DateTime CreateDate { get; set; }

        public int UserId { get; set; }
        public string UserName { get; set; }
        public List<Comments> Comments { get; set; }

    }
}
