﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeedBack.Entity
{
   public class FeedBackCount
    {
        public int FeedBackId { get; set; }
        public int CommentId { get; set; }
        public int PostId { get; set; }
        public int UserId { get; set; }
        public int Interest { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
